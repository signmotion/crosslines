#include "stdafx.h"
#include "CrossLines.h"


namespace crosslines {


CrossLines::CrossLines( const std::string& file ) {

    // ������ ����
    std::ifstream  fs( file );
    if ( !fs.is_open() ) {
        throw Exception( "���� '" + file + "' �� ������." );
    }
    typedef std::istreambuf_iterator< char >  buf_t;
    mData = std::string( buf_t( fs ),  buf_t() );
    if ( mData.empty() ) {
        throw Exception( "��� ������." );
    }
}




CrossLines::~CrossLines() {
}




CrossLines::rings_t
CrossLines::rings() const {

    if ( mRings.empty() ) {
        calcRings();
    }

    return mRings;
}




void
CrossLines::calcRings() const {

    // ������� ���������� ����� �� data()
    points_t  points;
    std::istringstream  iss( mData );
    do {
        std::string  x, y;
        iss >> x >> y;
        std::cout << "������  '" << x << "' '" << y << "'\n";
        if ( x.empty() || y.empty() ) {
            // # ������ ������ ����������.
            std::cout << "\t���������\n";
            continue;
        }
        coord_t  coord =
            { std::stod( x ), std::stod( y ), false, false };
        if ( DEBUG_SCALE > 1 ) {
            // # ��� ������� ������� ���������� � ������ �������.
            coord.x = round( coord.x * DEBUG_SCALE );
            coord.y = round( coord.y * DEBUG_SCALE );
        }
        std::cout << "\t������� ���  " << coord <<
            ((DEBUG_SCALE > 1) ? " (��. 'DEBUG_SCALE' � 'configure.h')" : "") <<
            "\n";
        points.push_back( coord );
    } while ( iss );

    if (points.size() < 3) {
        throw Exception( "���� �����." );
    }


    // ������ ��������� ���������
    {
        // # ������������� ��������� std::set::insert().
        std::set< coord_t, CoordCompare >  ts;
        points.remove_if( [ &ts ] ( const coord_t& c ) {
            const bool  dublicate = !ts.insert( c ).second;
            return dublicate;
        } );
    }


    // �������� ������
    points.push_back( points.front() );


    // ����� ��� ����������� �����
    // �������� ����� ������� �����������
    // ������� � ��������� ������ (��� ������ �����)
    points_t  extPoints;
    {
        auto atr1 = std::begin( points );
        auto atr2 = atr1;
        ++atr2;
        for ( ; atr2 != std::end( points ); ++atr1, ++atr2 ) {
            if ( (atr1 == std::begin( points )) || !(extPoints.back() == *atr1)) {
                extPoints.push_back( *atr1 );
            }
            const line_t  lineA = { *atr1, *atr2 };
            std::cout << "\n�����  " << lineA << "\n";
            std::cout << "\tX\n";
            auto btr1 = std::begin( points );
            auto btr2 = btr1;
            ++btr2;
            const CoordCompare  compare( *atr1 );
            std::set< coord_t, CoordCompare >  orderedCrossPoints( compare );
            for ( ; btr2 != std::end( points ); ++btr1, ++btr2 ) {
                if ( (*atr1 == *btr1) || (*atr2 == *btr2)
                  || (*atr1 == *btr2) || (*atr2 == *btr1)
                ) { continue; }
                const line_t  lineB = { *btr1, *btr2 };
                std::cout << "\t�����  " << lineB << "\n";
                coord_t  crosspoint;
                const bool  cross = intersection( lineA, lineB, &crosspoint );
                if ( cross ) {
                    std::cout << "\t\t������������ �  " << crosspoint << "\n";
                    // # ����� ����������� ���������� �� "�����������" ����� (atr1 -> atr2).
                    orderedCrossPoints.insert( crosspoint );
                }

            } // for ( ; btr2 ...

            // ������� ������������� �����
            for ( const auto& oc : orderedCrossPoints ) {
                const coord_t  c = { oc.x, oc.y, false, true };
                extPoints.push_back( c );
            }

            extPoints.push_back( *atr2 );

        } // for ( ; atr2 ...
    }


    // ��������� ������ � ������� �� � ���
    // # ��������� �� ����� � �����. ����� ����������� - �������, ��� ����
    //   ����������� ������ �����.
    // # �������������� ��� ������ ����� �������� ��� 'used'.

    // @return �������� �� �� �� ����� �� ������ ����� + 1 (����. ����� �����������
    //         ����� �� ������ �����).
    const auto  findAfterCross = [ &extPoints ] ( const points_t::iterator& itr ) {
        // # ��������� �� ����� ������, ����� �� ��������� ��������.
        for ( auto ftr = extPoints.begin(); ftr != extPoints.end(); ++ftr ) {
            if ( (ftr != itr) && (*ftr == *itr) ) { return ++ftr; }
        }
        return extPoints.end();
    };

    const auto  has = [] ( const ring_t& r, const coord_t& c ) {
        const auto  ftr = std::find( r.cbegin(), r.cend(), c );
        return (ftr != r.cend());
    };

    ring_t  ring;
    for ( auto itr = extPoints.begin(); itr != extPoints.end(); ++itr ) {
        if ( itr->used ) {
            continue;
        }
        ring.push_back( *itr );
        itr->used = true;
        if ( itr->cross ) {
            // ������� �� ��. ����� (���� ��������� ������)
            for ( auto ntr = findAfterCross( itr ); ntr != extPoints.end(); /**/ ) {
                DASSERT( ntr != extPoints.end() );
                ntr->used = true;
                if ( has( ring, *ntr ) ) {
                    // ������ ����������: ��������, ����������
                    ring.push_back( *ntr );
                    mRings.push_back( ring );
                    ring.clear();
                    break;
                }
                ring.push_back( *ntr );
                if ( ntr->cross ) {
                    ntr = findAfterCross( ntr );
                    DASSERT( ntr != extPoints.end() );
                } else {
                    ++ntr;
                }

            } // for ( auto ntr = ...

        } // if ( itr->cross )

        // ���������� ����� �����
        
    } // for ( auto itr = ...


    if ( DEBUG_SCALE > 1 ) {
        // # ����, ���� ������� �������, ����������� �������. ����������.
        for ( auto& ring : mRings ) {
            for ( auto& point : ring ) {
                point.x /= DEBUG_SCALE;
                point.y /= DEBUG_SCALE;
            }
        }
    } // if ( DEBUG_SCALE > 1 )
}








bool
intersection(
    const line_t&  l1,
    const line_t&  l2,
    coord_t*  intersectionPoint
) {
    // @source http://stackoverflow.com/a/14987452/963948
    // @source http://stackoverflow.com/a/565282/202451

    const coord_t  p = l1.a;
    const coord_t  q = l2.a;
    const coord_t  r = l1.b - l1.a;
    const coord_t  s = l2.b - l2.a;
    const real_t   srCross = cross( r, s );
    const coord_t  qp = q - p;
    const real_t   t = cross( qp, s ) / srCross;
    const real_t   u = cross( qp, r ) / srCross;
    if ( (t < 0) || (t > 1.0) || (u < 0) || (u > 1.0) ) {
        if ( intersectionPoint ) {
            *intersectionPoint = {};
        }
        return false;
    }

    if ( intersectionPoint ) {
        const coord_t  rt = { r.x * t, r.y * t };
        *intersectionPoint = p + rt;
    }
    return true;
}




real_t
cross( const coord_t& a, const coord_t& b ) {
    return a.x * b.y - a.y * b.x;
}




template< typename RealT >
int
round( RealT v ) {
    return static_cast< int >(std::floor( v + 0.5f ));
}




bool
operator==(const coord_t& a, const coord_t& b) {
    static const real_t  EPSILON = 0.001f;
    return (std::abs( a.x - b.x ) <= EPSILON)
        && (std::abs( a.y - b.y ) <= EPSILON);
}


coord_t
operator+(const coord_t& a, const coord_t& b) {
    return { a.x + b.x,  a.y + b.y };
}


coord_t
operator-(const coord_t& a, const coord_t& b) {
    return { a.x - b.x, a.y - b.y };
}


std::ostream& operator<<(std::ostream& out, const coord_t& c) {
    const auto  rp = out.precision();
    out.precision( 3 );
    out << c.x << "\t" << c.y;
    out.precision( rp );
    return out;
}


std::ostream& operator<<(std::ostream& out, const line_t& l) {
    return out << l.a << " --> " << l.b;
}


std::ostream& operator<<(std::ostream& out, const CrossLines::rings_t& rings) {
    
    size_t  n = 1;
    for ( const auto& ring : rings ) {
        out << "\n**" << n << "**\n";
        for ( const auto& point : ring ) {
            out << "\t" << point << "\n";
        }
        ++n;
    }

    return out;
}


} // crosslines
