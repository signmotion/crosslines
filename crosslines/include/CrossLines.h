#pragma once

#include "configure.h"


namespace crosslines {


typedef struct coord_t {
    real_t  x;
    real_t  y;
    // # ���������� ��� �� ��������� ��� �������� ��������. ����������.
    // �������, ��� ����� ��� ��������� � ������
    // @see calcRings()
    bool  used;
    bool  cross;
} coord_t;


typedef struct line_t {
    coord_t  a;
    coord_t  b;
} line_t;




class CrossLines {
public:
    typedef std::string  data_t;
    typedef std::list< coord_t >    points_t;
    typedef std::vector< coord_t >  ring_t;
    typedef std::list< ring_t >     rings_t;


public:
    CrossLines( const std::string& file );
    virtual ~CrossLines();

    // @throw std::exception ���� �������� ���������� ����� �� ������� ���
    //        � ����� ���� �����.
    rings_t rings() const;

    // @return ����� ������ �� �����.
    std::string const& data() const { return mData; }

private:
    void calcRings() const;


private:
    // ����� ������ �� �����
    data_t  mData;

    // ��� ��� rings()
    mutable rings_t  mRings;
};








bool intersection(
    const line_t&,
    const line_t&,
    coord_t*  intersectionPoint = nullptr
);


real_t cross( const coord_t&, const coord_t& );


template< typename RealT >
int round( RealT );




bool operator==(const coord_t&, const coord_t&);

coord_t operator+(const coord_t&, const coord_t&);
coord_t operator-(const coord_t&, const coord_t&);


std::ostream& operator<<( std::ostream&, const coord_t& );

std::ostream& operator<<(std::ostream&, const line_t&);

std::ostream& operator<<(std::ostream&, const CrossLines::rings_t&);




// ��� std::set< coord_t >
struct CoordCompare {
    const coord_t  point;

    CoordCompare( const coord_t& p = {} ) : point( p ) {}

    bool operator()( const coord_t& a, const coord_t& b ) const {
        const auto  distMagn = [ this ] ( const coord_t& t ) {
            const coord_t  d = point - t;
            return d.x * d.x + d.y * d.y;
        };
        return distMagn( a ) < distMagn( b );
    }
};


} // crosslines
