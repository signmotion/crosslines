#include "stdafx.h"
#include "CrossLines.h"


void runTest( const std::string& fileName );


// ���������� ������, ���������� ������������ �����.
// @see ����� 'doc'.
int
main( int argc, char** argv ) {

    setlocale( LC_ALL, "Russian" );
    // ��� ����������� '.' ������ ','
    setlocale( LC_NUMERIC, "C" );


    // ������ ���. ������
    const std::string  fileName = (argc < 2) ? "" : argv[ 1 ];
    int  retcode = 0;
    try {
        runTest( fileName );

    } catch ( const std::exception& ex ) {
        std::cout << "(!) " << ex.what() << "\n";
        retcode = -1;

    } catch ( ... ) {
        std::cout << "(!) Unknown exception.\n";
        retcode = -2;
    }


    std::cout << "\n^\n";
    std::cin.ignore();

    return retcode;

} // main()




void
runTest( const std::string& fileName ) {

    using namespace crosslines;

    if ( fileName.empty() ) {
        throw Exception( "������� ��� ����� ������ �� ����� '" + pathToResources + "'." );
        return;
    }

    const std::string  pathToFile = pathToResources + fileName;
    const CrossLines  cl( pathToFile );
    std::cout << "������ �� ����� '" << pathToFile << "'\n[" << cl.data() << "]\n\n";

    const auto  rings = cl.rings();
    std::cout << "\n\n**��������� ������**\n" << rings << std::endl;
}
